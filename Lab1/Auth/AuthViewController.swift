//
//  AuthViewcontroller.swift
//  Lab1
//
//  Created by Давид on 23.06.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import CryptoSwift

class AuthViewController: UIViewController {
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var auth: Auth!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func viewDidAppear() {
        super.viewDidAppear(true)
    }
    
    @IBAction func authButtonClick(sender: UIButton) {
        if loginTextField.text!.isEmpty || passwordTextField.text!.isEmpty {
            
        } else {
            saveTapped()
        }
    }
        
    func saveTapped() {
                    do {
                        let headers : HTTPHeaders = [
                                               "Content-Type" : "application/json"
                                           ]
                        
                        let parameters: [String: Any] = [
                                "grant_type" : "password",
                                "username": loginTextField.text,
                                "password" : passwordTextField.text
                            ]
                        
                        
                        
                        let todoEndpoint: String = "https://gitlab.com/oauth/token"
                        
                        AF.request(todoEndpoint, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                            switch response.result {
                            case .success:
                                if let value = response.data {
                                    var dataJson = JSON(value)
                                    do {
                                        
                                      self.auth = try JSONDecoder().decode(Auth.self, from: value)
                                      print(self.auth)
                                        guard let acctoken = self.auth.accessToken else { return }
                                        token = acctoken.encrypt()
                                        UserDefaults.standard.set(token, forKey: "Token")
                                        let dataView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DataTableViewController") as! DataTableViewController
                                        self.navigationController?.pushViewController(dataView, animated: true)
                                    } catch let jsonErr {
                                        print("Error serializing json:", jsonErr)
                                    }
                                }
                            case .failure(let error):
                                break
                            }
                        }
                    } catch  {
                }
            self.navigationController?.popViewController(animated: true)
        }
}
    
extension String {
    func encrypt() -> String?  {
        if let aes = try? AES(key: "itsreallystrongpassword", iv: "reallystrong"),
           let encrypted = try? aes.encrypt(Array(self.utf8)) {
            return encrypted.toHexString()
        }
        return nil
    }

    func decrypt() -> String? {
        if let aes = try? AES(key: "itsreallystrongpassword", iv: "reallystrong"),
            let decrypted = try? aes.decrypt(Array<UInt8>(hex: self)) {
            return String(data: Data(bytes: decrypted), encoding: .utf8)
        }
        return nil
    }
}

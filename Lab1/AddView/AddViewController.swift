//
//  AddViewcontroller.swift
//  Lab1
//
//  Created by Давид on 22.06.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class AddViewController: UIViewController {
    
    var issues: Base!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
        
    }
    
    func viewDidAppear() {
        super.viewDidAppear(true)
        
        
    }
    
    @objc func saveTapped() {
                do {
                   let headers : HTTPHeaders = [
                    "Authorization" : "Bearer \(token.decrypt()!)"
                    ]
                    
                    let todoEndpoint: String = "https://gitlab.com/api/v4/projects/\(issues.project_id!)/issues?title=\(titleField.text!.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)&description=\(descriptionField.text!.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)"
                    
                    AF.request(todoEndpoint, method: .post, encoding: URLEncoding.default, headers: headers).responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.data {
                                var dataJson = JSON(value)
                            }
                        case .failure(let error):
                            break
                        }
                    }
                } catch  {
            }
        self.navigationController?.popViewController(animated: true)
    }
}

import Foundation

struct Base : Codable {
	let id : Int!
	let iid : Int!
	let project_id : Int!
	let title : String!
	let description : String!
	let state : String!

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case iid = "iid"
		case project_id = "project_id"
		case title = "title"
		case description = "description"
		case state = "state"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		iid = try values.decodeIfPresent(Int.self, forKey: .iid)
		project_id = try values.decodeIfPresent(Int.self, forKey: .project_id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		state = try values.decodeIfPresent(String.self, forKey: .state)
	}

}

//
//  ViewController.swift
//  Lab1
//
//  Created by Давид on 21.06.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

var token: String!

class DataTableCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
}

class DataTableViewController: UITableViewController {
    
    var messageFromServer: String!
    var issues: [Base]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        sendFormRequest(complete: {})
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        self.title = "Issues"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Exit", style: .plain, target: self, action: #selector(exitTapped))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        sendFormRequest(complete: {})
    }
    
    @objc func exitTapped() {
        token = nil
        UserDefaults.standard.removeObject(forKey: "Token")
        let authView = UIStoryboard(name: "AuthViewController", bundle: nil).instantiateViewController(withIdentifier: "AuthViewController") as! AuthViewController
        authView.modalPresentationStyle = .fullScreen
        self.present(authView, animated: true, completion: nil)
    }
    
    @objc func addTapped() {
        let addView = UIStoryboard(name: "AddViewController", bundle: nil).instantiateViewController(withIdentifier: "AddViewController") as! AddViewController
        addView.issues = self.issues[0]
        navigationController?.pushViewController(addView, animated: true)
    }
    
    @objc func refresh(refreshControl: UIRefreshControl) {
       sendFormRequest(complete: {})
       refreshControl.endRefreshing()
    }
    
    func sendFormRequest(complete: @escaping () -> Void){
        let todoEndpoint: String = "https://gitlab.com/api/v4/issues"
               let headers : HTTPHeaders = [
                "Authorization" : "Bearer \(token.decrypt()!)"
               ]
        AF.request(todoEndpoint, method: .get, encoding: URLEncoding.default, headers: headers).responseJSON { response in
                   switch response.result {
                   case .success:
                       if let value = response.data {
                           var dataJson = JSON(value)
                            print(dataJson)
                                      
                                      do {
                                          
                                        self.issues = try JSONDecoder().decode([Base].self, from: value)
                                        self.tableView.reloadData()
                                        print(self.issues)
                                      } catch let jsonErr {
                                          print("Error serializing json:", jsonErr)
                                      }
                                
                                
                                
                           complete()
                       }
                   case .failure(let error):
                       complete()
                   }
        }
    }
    
    func deleteIssue(projects_id: String!, iid: String!) {
            do {
               let headers : HTTPHeaders = [
                    "PRIVATE-TOKEN": token!
                ]
                
                let todoEndpoint: String = "https://gitlab.com/api/v4/projects/\(projects_id!)/issues/\(iid!)"
                
                AF.request(todoEndpoint, method: .delete, encoding: URLEncoding.default, headers: headers).responseJSON { response in
                    switch response.result {
                    case .success:
                        if let value = response.data {
                            var dataJson = JSON(value)
                        }
                    case .failure(let error):
                        break
                    }
                }
            } catch  {
        }
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let issues = issues else {
            return 1
        }
        
        return  issues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Data") as! DataTableCell
        guard let issues = issues else {
            return cell
        }
        cell.titleLabel.text = issues[indexPath.row].title
        cell.dataLabel.text = issues[indexPath.row].state
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alertController = UIAlertController(title: "Delete issues", message:"", preferredStyle: UIAlertController.Style.alert)
            let cancelAction = UIAlertAction(title: "Cancel", style:UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in }
            let okAction = UIAlertAction(title: "OK", style:UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                self.deleteIssue(projects_id: self.issues[indexPath.row].project_id.description, iid: self.issues[indexPath.row].iid.description)
                self.issues.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailView = UIStoryboard(name: "DetailViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailData") as! DetailDataViewController
        detailView.issues = self.issues[indexPath.row]
        navigationController?.pushViewController(detailView, animated: true)
    }


}


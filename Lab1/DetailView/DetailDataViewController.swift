//
//  DetailDataViewController.swift
//  Lab1
//
//  Created by Давид on 22.06.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class DetailDataCell: UITableViewCell {
    @IBOutlet weak var titleDetail: UILabel!
    @IBOutlet weak var valueDetail: UILabel!
}

class DetailDataDescription: UITableViewCell {
    @IBOutlet weak var descriptionView: UITextView!
}

class DetailDataViewController: UITableViewController, UITextViewDelegate {
    
    var issues: Base!
    var issuesArr: Array! = ["id", "iid", "title", "projectId", "state"]
    var issuesValueArr: Array! = []
    var descriptionText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        issuesValueArr.append(issues.id.description)
        issuesValueArr.append(issues.iid.description)
        issuesValueArr.append(issues.title)
        issuesValueArr.append(issues.project_id.description)
        issuesValueArr.append(issues.state)
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: .zero)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    @objc func saveTapped() {
                do {
                   let headers : HTTPHeaders = [
                        "Authorization" : "Bearer \(token.decrypt()!)"
                    ]
                    
                    let todoEndpoint: String = "https://gitlab.com/api/v4/projects/\(issues.project_id!)/issues/\(issues.iid!)?description=\(descriptionText!.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)"
                    
                    AF.request(todoEndpoint, method: .put, encoding: URLEncoding.default, headers: headers).responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.data {
                                var dataJson = JSON(value)
                            }
                        case .failure(let error):
                            break
                        }
                    }
                } catch  {
            }
        self.navigationController?.popViewController(animated: true)
    }

    func textViewDidChange(_ textView: UITextView) {
        // Animated height update
        DispatchQueue.main.async {
            self.descriptionText = textView.text
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.tableView?.beginUpdates()
            self.tableView?.endUpdates()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 5
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Description"
        } else {
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailDataCell", for: indexPath) as! DetailDataCell
            cell.titleDetail.text = issuesArr[indexPath.row]
            cell.valueDetail.text = issuesValueArr[indexPath.row] as! String
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailDataDescription", for: indexPath) as! DetailDataDescription
            cell.descriptionView.text = issues.description
            cell.descriptionView.delegate = self
            return cell
        }
    }
    
}

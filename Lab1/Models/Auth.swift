//
//  Auth.swift
//  Lab1
//
//  Created by Давид on 24.06.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import Foundation


struct Auth: Codable {
    let accessToken: String!
    
    enum CodingKeys: String, CodingKey {

        case accessToken = "access_token"
    }
}
